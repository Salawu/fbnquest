// Your web app's Firebase configuration
  var firebaseConfig = {
    apiKey: "AIzaSyCml4TfMw9q6TOyxdFgYRTOKjo6ghqyrCk",
    authDomain: "fbnquest-79747.firebaseapp.com",
    databaseURL: "https://fbnquest-79747.firebaseio.com",
    projectId: "fbnquest-79747",
    storageBucket: "fbnquest-79747.appspot.com",
    messagingSenderId: "479183338168",
    appId: "1:479183338168:web:df52b44e38e5286c3d524d",
    measurementId: "G-282TKYJS2M"
};
  // Initialize Firebase
  firebase.initializeApp(firebaseConfig);
  firebase.analytics();

//   var corporatebankRef = firebase.database().ref();
  var rootRef = firebase.database().ref('corporatebanking');

//   document.getElementById('msform').addEventListener('submit', submitForm);
  document.getElementById("submit").addEventListener("click", submitForm);

  // Submit form
function submitForm(e){

    e.preventDefault();

    var businessName  = getInputVal('bn');
    var certificateOfIncorporation  = getInputVal('cof');
    var jurisdictionOfIncorporation = getInputVal('joi');
    var businessType = getInputVal('bt');
    var operatingBusinessAddress = getInputVal('oba');
    var secondaryBusinessAddress = getInputVal('sba');
    var emailAddress = getInputVal('email');
    var title = getInputVal('title');
    var fullName = getInputVal('name');
    var nationality = getInputVal('nationality');
    var placeOfBirth = getInputVal('pob');
    var bankVerificationNo = getInputVal('bvn');
    var occupation = getInputVal('cpass');
    var residentialAddress = getInputVal('ra');
    var directorOrCompanySecretarysName = getInputVal('dcsn');
    var directorsNameContactNumber = getInputVal('cn');
    var declear = getInputCheck('declearation');

        
    console.log(declear);

    if(businessName != "" && certificateOfIncorporation !="" && jurisdictionOfIncorporation !="" && businessType != "" && operatingBusinessAddress !=""
        && secondaryBusinessAddress !="" && emailAddress != "" && title != "" && fullName != "" && nationality != "" && placeOfBirth != "" && bankVerificationNo != ""
        && occupation !="" && residentialAddress != "" && directorOrCompanySecretarysName != "" && directorsNameContactNumber != "" && declear != false){

          
         // Save message
    saveAccount(businessName, certificateOfIncorporation, jurisdictionOfIncorporation, businessType, operatingBusinessAddress,secondaryBusinessAddress,emailAddress,
      title,fullName,nationality,placeOfBirth,bankVerificationNo,occupation,residentialAddress,directorOrCompanySecretarysName,directorsNameContactNumber);

     

     toastr.success('Account creation successful. Thank you');

       document.getElementById('msform').reset();


        setTimeout(function(){
        window.location.replace("index.html");
         },3000); 

    }else{

      toastr.error("All fields are required");


    }
  
    

   
  }
  // Function to get get form values

  // function validateForm() {
  //   var x = document.forms["myForm"]["fname"].value;
  //   if (x == "") {
  //     alert("Name must be filled out");
  //     return false;
  //   }
  // } 


function getInputCheck(id){

  var x = document.getElementById(id).checked;

  if(x){

    return x;
  }else{
    toastr.error("Please indicate that you accept the declearation");
    return x;

  }
    
  }

function getInputVal(id){

  var x = document.getElementById(id).value;

    return x;
  }

  
  // Save message to firebase
  function saveAccount(businessName, certificateOfIncorporation, jurisdictionOfIncorporation, businessType, operatingBusinessAddress,secondaryBusinessAddress,
                       emailAddress,title,fullName,nationality,placeOfBirth,bankVerificationNo,occupation,residentialAddress,directorOrCompanySecretarysName,directorsNameContactNumber){
    var newCorporatebankRef = rootRef.push();
    newCorporatebankRef.set({
        businessName: businessName,
        certificateOfIncorporation:certificateOfIncorporation,
        jurisdictionOfIncorporation:jurisdictionOfIncorporation,
        businessType:businessType,
        operatingBusinessAddress:operatingBusinessAddress,
        secondaryBusinessAddress:secondaryBusinessAddress,
        emailAddress:emailAddress,
        title:title,
        fullName:fullName,
        nationality:nationality,
        placeOfBirth:placeOfBirth,
        bankVerificationNo:bankVerificationNo,
        occupation:occupation,
        residentialAddress:residentialAddress,
        directorOrCompanySecretarysName:directorOrCompanySecretarysName,
        directorsNameContactNumber:directorsNameContactNumber
    });
  }
  
